<?php
include('include/Box.php');
include('include/Calculator.php');
$calculator = new Calculator();
$width = '';
$depth = '';
$height = '';
$volume = null;
if (isset($_POST["submit"]))
{
    $width = is_numeric($_POST["width"]) ? $_POST["width"] : '';
    $depth = is_numeric($_POST["depth"]) ? $_POST["depth"] : '';
    $height = is_numeric($_POST["height"]) ? $_POST["height"] : '';
    if ($width && $depth && $height) {
        $box = new Box($width,$depth,$height);
        $calculator->setBox($box);
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <style>
        .form-row {
            margin-top: 5px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="jumbotron text-center">
        <h2>Bereken het oppervlak en de inhoud</h2>
        <?php if ($calculator->hasBox()): ?>
            <h3>Het oppervlak is <?php echo $calculator->getSurface(); ?> cm<sup>2</sup></h3>
            <h3>Het volume is <?php echo $calculator->getVolume(); ?> cm<sup>3</sup></h3>
        <?php endif; ?>
    </div>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <div class="row form-row">
            <div class="col-xs-6 text-right">
                <label for="width">Breedte</label>
            </div>
            <div class="col-xs-3">
                <input class="form-control" type="text" id="width" name="width" value="<?php echo $width; ?>" placeholder="geef de breedte in cm"><br>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-6 text-right">
                <label for="depth">Diepte</label>
            </div>
            <div class="col-xs-3">
                <input class="form-control" type="text" id="depth" name="depth" value="<?php echo $depth; ?>" placeholder="geef de diepte in cm"><br>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-6 text-right">
                <label for="height">Hoogte</label>
            </div>
            <div class="col-xs-3">
                <input class="form-control" type="text" id="height" name="height" value="<?php echo $height; ?>" placeholder="geef de hoogte in cm"><br>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-6 text-right">

            </div>
            <div class="col-xs-6">
                <input type="submit" name="submit" value="Bereken het volume">
            </div>
        </div>
    </form>
</div>
</body>
</html>